/*
Any copyright is dedicated to the Public Domain.
https://creativecommons.org/publicdomain/zero/1.0/

This is a port of really crap Python program I made years ago. This proberly better code than the Python version even though this 
is my first ever Javascript program that wasn't just copying examples from MDN. I guess my coding has gotten a little less crud in
general.

I am not gonna be using JS outside of the web because no sane person does that. Come on, it was made in TEN DAYS by ONE Netscape
employee! Which is pretty inpressive but it shows to this day from what I have heard. What I do know is that for some reason
'2' == 2 gives true; what is this, Scratch? Also it has nothing to do with Java, it was orginaly going to be called Livescript
but some Netspace executives forced it to be called Javascript as some sort of marketing ploy. Clearly, the 90s were a different
time. This blather is proberly almost the same size as the actual code so I will stop now. I wonder if anyone will ever read this? 
*/
const name = ['Bob', 'Eve', 'Alice', 'John', 'Sam', 'Mary', 'Bob', 'Joe', 'Nobody', 'Disney', 'Samsung', 'Google', 'Chris', 'Everybody', 'Percy'];
const verb = ['rides', 'kicked', 'ate', 'bought', 'eats', 'broke', 'bought and then ate', 'killed', 'dropped', 'sued', 'was eaten by', 'is eating', 'is being sued by', 'played', 'is playing', 'is playing on', 'was hit by'];
const noun = ['a lion', 'a bicycle', 'a plane', 'a computer', 'a phone', 'a tractor','Bob', 'Eve', 'Alice', 'John', 'Sam', 'Mary', 'Bob', 'Joe', 'a ball', 'a fox', 'a cat', 'a dog', 'a banana', 'a fidget cube', 'a fidget spinner', 'an apple', 'an iPad', 'a tablet', 'a Raspberry Pi', 'Google', 'Disney', 'Samsung', 'the phone', 'mari0', 'Not Pacman', 'Chris', 'everyone', 'Percy'];
const btn = document.querySelector('#randomSentenceBtn');
const list = document.querySelector('#randomSentenceTxt');

function randint(min, max) {
  // I 'borrowed' this from MDN. What? Everyone does it!
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function pick(words) {
  // This is only here to make the code easier to read.
	return words[randint(0, words.length)];
}

btn.onclick = function(){
	var newItem = document.createElement('li');
	newItem.textContent = pick(name) + ' ' + pick(verb) + ' ' + pick(noun) + '.';
	list.appendChild(newItem);
	console.log(newItem.textContent);
}
