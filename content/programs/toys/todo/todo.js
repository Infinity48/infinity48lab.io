let list = document.querySelector('#list');
let input = document.querySelector('#textBox');
let form = document.querySelector('#input');
let clear = document.querySelector('#clearBtn');
let todos = [];

function addToList(text, id) {
    let newItem = document.createElement('li');
    newItem.textContent = text;
    newItem.id = id;
    newItem.onclick = removeFromList;
    list.appendChild(newItem);
}

function removeFromList() {
    todos.splice(event.target.id, 1);
    event.target.remove();
    window.localStorage.setItem('todos', JSON.stringify(todos));
}

window.onload = function() {
    let oldTodos = JSON.parse(window.localStorage.todos);
    if (oldTodos) {todos = oldTodos}
    for (key in todos) {
        addToList(todos[key], key);
    }
}

form.onsubmit = function() {
    if (input.value != '') {
        todos.push(input.value);
        addToList(input.value, todos.length - 1);
        window.localStorage.setItem('todos', JSON.stringify(todos));
    }
    input.value = '';
    event.preventDefault();
}

clear.onclick = function() {
    if (confirm('Are you sure you want to delete your todo list? (This cannot be undone!)')) {
        input.value = '';
        window.localStorage.setItem('todos', '[]');
        window.location.reload();
    }
}