+++
title = 'About me'
date = 2024-12-31T11:55:10Z
draft = false
+++
Hi, I'm Infinity, I ~~like~~ am interested in computers and this is my website. It is currently on a subdomain of Gitlab but I intend to buy a domain at some point.
### Some facts about me
- 18 years old.
- Autisic.
- Member of the species *Homo sapiens*.
- English, unfortunally.
- Massive nerd.
- Knows the following programming languages (from most to least):
    - Python
	- GDScript
	- JavaScript (regretably)
	- C#
	- C
	- Lua
	- I can 'Hello World' in a few others but that is about it.
### Opinions
- Windows 7 was the last half decent version of Windows, 2000 was the first.
- Systemd is a fine service manager attached at the hip to a bunch of crap.
- Most tech sectors are 90% bullshit, AI and Cryptocurrency is 99.9% bullshit.
- Eat the rich.
### Some of my favourite video games (in no particular order)
- Paper Mario: The Thousand-Year Door
- Super Paper Mario
- [Deltarune](https://deltarune.com/)
- [Portal 2](https://store.steampowered.com/app/620/Portal_2/)
### Me on other sites
- [GitLab](https://gitlab.com/Infinity48)
- [GameJolt](https://gamejolt.com/@Infinity48)
- [itch.io](https://infinity48.itch.io/)
- [Youtube (I seldom upload)](https://www.youtube.com/@infinity48)
- [mstdn.social](https://mstdn.social/@infinity48)